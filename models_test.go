package main

import (
	"fmt"
	"testing"

	"github.com/jinzhu/gorm"
)

func TestShortPass(t *testing.T) {
	pass := generateShortPass()
	if len(pass) != 4 {
		t.Errorf("not equal 4 its %v", len(pass))
	}
	t.Log(fmt.Sprintf("Pass is: %v", pass))
}

func TestAuth(t *testing.T) {
	authUser("88005553535", "6832")
}

func TestDecodeUserHash(t *testing.T) {
	auth, _ := authUser("88005553535", "6832")
	user := User{}
	db, _ := gorm.Open("postgres", Conf.DbConnetionString())
	db.First(&user, "phone=?", "88005553535")
	if user.UserHash == auth {
		t.Logf("ok equals")
	} else {
		t.Fatalf("no")
	}
}
func TestLogout(t *testing.T) {
	auth, _ := authUser("88005553535", "6832")
	Logout(auth)
}
