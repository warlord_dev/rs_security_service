package main

import (
	"crypto/md5"
	"crypto/tls"
	"encoding/hex"
	"errors"
	"fmt"

	"math/rand"
	"strconv"
	"time"

	"gopkg.in/gomail.v2"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func init() {
	dBSesion().AutoMigrate(&User{})
}

//User - user entity
type User struct {
	gorm.Model
	Name       string
	Lastname   string
	SureName   string
	Birthday   time.Time
	Phone      string `gorm:"type:varchar(100);unique_index"`
	Email      string
	ShortPass  string
	Role       string `gorm:"size:255"`        // set field size to 255
	AutoNumber string `gorm:"unique;not null"` // set member number to unique and not null
	Address    string `gorm:"index:addr"`      // create index with name `addr` for address
	UserHash   string
}

//Private

func dBSesion() *gorm.DB {
	fmt.Println(Conf.DbConnetionString())
	db, err := gorm.Open("postgres", Conf.DbConnetionString())
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}
	return db
}

func createUser(t User) {
	t.ShortPass = generateShortPass()
	err := dBSesion().Save(&t).Error
	if err != nil {
		panic("create user pannic")
	}
	t.sendToEmail()
}

func (t User) registrateUser() (string, error) {
	_, err := userValidation()
	if err != nil {
		return "", nil
	}
	createUser(t)
	return "ok", nil
}

func (t User) sendToEmail() {
	d := gomail.NewDialer(Conf.EmailHost, Conf.EmailPort, Conf.EmailLogin, Conf.EmailPassword)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	m := gomail.NewMessage()
	m.SetHeader("From", Conf.EmailLogin)
	m.SetHeader("To", t.Email)
	m.SetHeader("Subject", "Security")
	m.SetHeader("Subject", fmt.Sprintf("Hello! its %v", Conf.AppName))
	m.SetBody("text/html", fmt.Sprintf("Hello your pass is <b>%v</b>!", t.ShortPass))
	err := d.DialAndSend(m)
	if err != nil {
		panic(err)
	}
	fmt.Println("ok sended")
}

func userValidation() (string, error) {
	return "ok", nil
}

func generateShortPass() string {
	return strconv.Itoa(rand.Intn(9999-1111) + 1111)
}

func authUser(phone string, pass string) (userHash string, err error) {
	user := User{}
	dBSesion().First(&user, "phone=?", phone)
	if user.ShortPass == pass {
		myHash := fmt.Sprint(user.ID)
		hasher := md5.New()
		hasher.Write([]byte(myHash))
		userHash := hex.EncodeToString(hasher.Sum(nil))
		dBSesion().Model(&user).Update("UserHash", userHash)
		return userHash, nil
	}
	return "", errors.New("Not authorize")
}

//Public

//GetAllUsers - return all users in table users
func GetAllUsers() (allusers []User, err error) {
	dBSesion().Table("users").Find(&allusers)
	if &allusers == nil {
		panic(fmt.Sprint("nil"))
	}
	err = nil
	return
}

//UserLogin - authorizate user in system
func UserLogin(phone string, pass string) string {
	data, _ := authUser(phone, pass)
	return data
}

//UserLogout - logout user in system
func UserLogout(userHash string) {
	user := User{}
	dBSesion().First(&user, "user_hash=?", userHash).Update("UserHash", "")
}
