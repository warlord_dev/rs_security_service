package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

//Registartion - registrate new user
func Registartion(w http.ResponseWriter, r *http.Request) {
	//user := User{Name: "stas", Birthday: time.Now(), Email: "test@gmail.com", ShortPass: "", Role: "1"}
	name := r.FormValue("Name")
	phone := r.FormValue("Phone")
	birthday, _ := time.Parse("2006-01-02T15", r.FormValue("Birthday"))
	email := r.FormValue("Email")
	shortpass := ""
	addres := r.FormValue("Addres")
	autoNumber := r.FormValue("AutoNumber")
	role := "1"
	user := User{Name: name, Birthday: birthday, Phone: phone, Email: email, ShortPass: shortpass,
		Role: role, Address: addres, AutoNumber: autoNumber}
	data, err := user.registrateUser()
	if err != nil {
		resData, _ := json.Marshal(err)
		fmt.Println("its eror: ", err)
		w.Header().Set("content-type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(resData)
	}
	resData, _ := json.Marshal(data)
	fmt.Println(data)
	w.Header().Set("content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write(resData)
}

//GetAll - return all Users
func GetAll(w http.ResponseWriter, r *http.Request) {
	data, err := GetAllUsers()
	if err != nil || len(data) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
	}
	resData, _ := json.Marshal(data)
	fmt.Println(data)
	w.Header().Set("content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write(resData)
}

//Login - is Login
func Login(w http.ResponseWriter, r *http.Request) {
	phone := r.FormValue("Phone")
	pass := r.FormValue("Pass")
	data := UserLogin(phone, pass)
	resData, _ := json.Marshal(data)
	fmt.Println(data)
	w.Header().Set("content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write(resData)
}

//Logout - is Logout
func Logout(w http.ResponseWriter, r *http.Request) {
	hash := r.FormValue("Hash")
	UserLogout(hash)
	resData, _ := json.Marshal("ok logout")
	w.Header().Set("content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write(resData)
}
