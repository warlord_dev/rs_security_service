package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

var config string

func init() {
	config = Conf.HTTPConnectionString()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/reg", Registartion).Methods("GET")
	r.HandleFunc("/all", GetAll).Methods("GET")
	r.HandleFunc("/login", Login).Methods("GET")
	r.HandleFunc("/logout", Logout).Methods("GET")
	http.Handle("/", r)
	log.Printf("server run! go to http://%v/", config)
	log.Fatal(http.ListenAndServe(config, handlers.LoggingHandler(os.Stdout, http.DefaultServeMux)))
}
