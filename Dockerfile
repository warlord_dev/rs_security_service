# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Copy the local package files to the container's workspace.
ADD . /go/src/bitbucket.org/renegatum/rs_security_service

# Build the outyet command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN go get github.com/gorilla/mux
RUN go get github.com/gorilla/handlers
RUN go get github.com/lib/pq
RUN go get github.com/jinzhu/gorm
RUN go get gopkg.in/gomail.v2
RUN go install bitbucket.org/renegatum/rs_security_service

# Run the outyet command by default when the container starts.
ENTRYPOINT /go/bin/rs_security_service

# Document that the service listens on port 9000.
EXPOSE 9000